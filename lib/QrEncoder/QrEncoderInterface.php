<?php
namespace Mgnl\QrEncoder;

interface QrEncoderInterface
{

    /**
     * Encodings
     */
    const ENCODING_DIGIT     = 0x0001;
    const ENCODING_ALPHA_NUM = 0x0010;
    const ENCODING_KANJI     = 0x0100;
    const ENCODING_BYTE      = 0x1000;
    const ENCODING_ECI       = 0x0111; // Extended Channel Interpretation (not supported yet)

    /**
     * Error Corrections
     */
    const ECC_LEVEL_L = 1;
    const ECC_LEVEL_M = 2;
    const ECC_LEVEL_Q = 3;
    const ECC_LEVEL_H = 4;

    /**
     * Version
     */
    const VERSION_MIN = 1;
    const VERSION_MAX = 40;

    /**
     * Count Indicators Lenght
     */
    const COUNT_INDICATOR_LENGHT = [

    ];
}
