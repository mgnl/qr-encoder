<?php

namespace Mgnl\QrEncoder;

use InvalidArgumentException;

final class QrInput
{

    /**
     * The flag for an auto discovery of the input type
     *
     * @const int
     */
    const AUTO = -1;

    /**
     * The input types numbers
     *
     * @const int
     */
    const DIGIT     = 1;
    const ALPHA_NUM = 2;
    const KANJI     = 3;
    const BYTE      = 4;
    const ECI       = 7; // Not supported yet

    /**
     * The text to encode
     *
     * @var string
     */
    private $text;

    /**
     * The input type numbers which are representing:
     * 1 - Digit
     * 2 - AlphaNum
     * 3 - Kanji
     * 4 - Byte
     *
     * @var int
     */
    private $type;

    /**
     * The handler of the specific input type object
     *
     * @var InputTypeInterface
     */
    private $handler;

    /**
     * Return the QR text to encode
     *
     * @return string
     */
    public function getText(): string
    {
        $this->text;
    }

    /**
     * Sets the text for QR encoding and resets properties
     *
     * @param string $text
     *
     * @return void
     */
    public function setText(string &$text): void
    {
        $this->text    = $text;
        $this->type    = null;
        $this->handler = null;
    }

    /**
     * Returns the input type number
     *
     * @return int
     */
    public function getType(): int
    {
        if (null === $this->type) {
            $this->setType();
        }

        return $this->type;
    }

    /**
     * Sets the input type
     *
     * @param int $type
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    public function setType(int $type = null): void
    {
        if (!in_array($type, [1, 2, 3, 4, self::AUTO, null], true)) {
            throw new InvalidArgumentException("Invalid data type parameter '$type'");
        }

        $inputType = $this->resolveInputType();

        if (self::AUTO === $type || null === $type) {
            $type = $inputType;
        } elseif ($inputType !== $type) {
            // @todo Log warninig. We allowed set valid but incorrect data type.
            // The results are unexpected, but if someone needs something like that,
            // let's do it for her or for him, but... log it dude log it ;)
        }

        $this->type = $type;
    }

    /**
     * Creates data object handler
     *
     * @param string $text
     *
     * @param int|null $type
     *
     * @return self
     */
    public function __construct(string &$text, int $type = null)
    {
        $this->text = &$text;

        if (null !== $type) {
            $this->setType($type);
        }
    }

    public function getBinnary(): string
    {
        return $this->getHandler()->getBinnary();
    }

    public function getTextLength(): int
    {
        return $this->getHandler()->getLength();
    }

    public function getBinnaryType(): string
    {
        return str_pad(decbin($this->type), 4, 0, STR_PAD_LEFT);
    }

    /**
     * @return int
     */
    private function resolveInputType(): int
    {
        if (InputType\Digit::isValid($this->text)) {
            return self::DIGIT;
        } elseif (InputType\AlphaNum::isValid($this->text)) {
            return self::ALPHA_NUM;
        } elseif (InputType\Kanji::isValid($this->text)) {
            return self::KANJI;
        } elseif (InputType\Byte::isValid($this->text)) {
            /**
             * This is always true
             * @todo please, do me ;)
             */
            return self::BYTE;
        }
    }

    private function getHandler(): InputType\InputTypeAbstract
    {
        if (null === $this->handler) {
            $type = $this->getType();

            if (self::DIGIT === $type) {
                $this->handler = new InputType\Digit($this->text);
            } elseif (self::ALPHA_NUM === $type) {
                $this->handler = new InputType\AlphaNum($this->text);
            } elseif (self::KANJI === $type) {
                $this->handler = new InputType\Kanji($this->text);
            } elseif (self::BYTE === $type) {
                $this->handler = new InputType\Byte($this->text);
            }
        }

        return $this->handler;
    }
}
