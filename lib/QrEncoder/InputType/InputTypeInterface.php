<?php

namespace Mgnl\QrEncoder\InputType;

interface InputTypeInterface
{

    /**
     * @param string $data
     *
     * @return bool
     */
    public static function isValid(string &$data): bool;

    /**
     * @param int $requiredBites
     *
     * @return string
     */
    public function getBinnary(): string;

    /**
     * @return int
     */
    public function getBinnaryLength(): int;

    /**
     * @return int
     */
    public function getLength(): int;
}
