<?php

namespace Mgnl\QrEncoder\InputType;

/**
 * @see https://www.thonky.com/qr-code-tutorial/kanji-mode-encoding
 * @see http://www.rikai.com/library/kanjitables/kanji_codes.sjis.shtml
 */
class Kanji extends InputTypeAbstract
{

    /**
     * @inhiredoc
     */
    public static function isValid(string &$data): bool
    {
        $kanji = mb_convert_encoding($data, 'SJIS');

        if (!preg_match('/[^\x{8140}-\x{9FFC}]/u', $kanji)) {
            return true;
        }

        return !preg_match('/[^\x{E040}-\x{EBBF}]/u', $kanji);
    }

    /**
     * @inhiredoc
     */
    public function getBinnary(): string
    {
        if (null === $this->binnary) {

            $this->binnary = '';

            $data = mb_convert_encoding($this->text, 'SJIS');

            $len = strlen($data);

            for ($i = 0; $i + 1 < $len; $i += 2) {

                $c = ((0xff & ord($data[$i])) << 8) | (0xff & ord($data[$i + 1]));

                if (0x8140 <= $c && $c <= 0x9FFC) {
                    $c -= 0x8140;
                } elseif (0xE040 <= $c && $c <= 0xEBBF) {
                    $c -= 0xC140;
                }

                $dec = (($c >> 8) & 0xff) * 0xC0 + ($c & 0xff);

                $this->binnary .= str_pad(decbin($dec), 13, 0, STR_PAD_LEFT);
            }
        }

        return $this->binnary;
    }

    /**
     * @inhiredoc
     */
    public function getLength(): int
    {
        return mb_strlen($this->text);
    }
}
