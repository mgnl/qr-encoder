<?php

namespace Mgnl\QrEncoder\InputType;

abstract class InputTypeAbstract implements InputTypeInterface
{

    /**
     * The text to encode
     *
     * @var string
     */
    protected $text = '';

    /**
     * The text length it depends on encoding (charset)
     *
     * @var int
     */
    protected $length = 0;

    /**
     * The converted data binnary string
     *
     * @var string
     */
    protected $binnary;

    /**
     * @inhiredoc
     */
    abstract public static function isValid(string &$data): bool;

    /**
     * @param string $text
     * @param int    $length
     *
     * @return self
     */
    public function __construct(string &$text)
    {
        $this->text = &$text;
    }

    /**
     * @inhiredoc
     */
    abstract public function getBinnary(): string;

    /**
     * @inhiredoc
     */
    public function getBinnaryLength(): int
    {
        return strlen($this->binnary);
    }
}
