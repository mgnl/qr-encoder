<?php

namespace Mgnl\QrEncoder\InputType;

class Byte extends InputTypeAbstract
{

    /**
     * @inhiredoc
     */
    public static function isValid(string &$data): bool
    {
        return true;
    }

    /**
     * @inhiredoc
     */
    public function getBinnary(): string
    {
        if (null === $this->binnary) {

            $this->binnary = '';

            foreach (str_split($this->text) as $char) {
                $this->binnary .= str_pad(decbin(ord($char)), 8, 0, STR_PAD_LEFT);
            }
        }

        return $this->binnary;
    }

    /**
     * @inhiredoc
     */
    public function getLength(): int
    {
        return mb_strlen($this->text);
    }
}
