<?php

namespace Mgnl\QrEncoder\InputType;

final class AlphaNum extends InputTypeAbstract
{

    /**
     * The alpha numeric characters collection
     *
     * @const array
     */
    const MAP = [
        '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'A', 'B', 'C', 'D', 'E', 'F',
        'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
        'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
        'W', 'X', 'Y', 'Z', ' ', '$', '%', '*',
        '+', '-', '.', '/', ':',
    ];

    /**
     * @inhiredoc
     */
    public static function isValid(string &$data): bool
    {
        return !preg_match('~[^A-Z0-9\$\%\\*\+\-\./\:\ ]~', $data);
    }

    /**
     * @inhiredoc
     */
    public function getBinnary(): string
    {
        if (null === $this->binnary) {

            $this->binnary = '';

            $splited = str_split($this->text, 2);

            for ($i = 0; $i < count($splited); $i++) {

                $first  = array_search($splited[$i][0], self::MAP);
                $second = isset($splited[$i][1]) ? array_search($splited[$i][1], self::MAP) : null;

                if (null === $second) {
                    $this->binnary .= str_pad(decbin($first), 6, 0, STR_PAD_LEFT);
                } else {
                    $number = $first * 45 + $second;

                    $this->binnary .= str_pad(decbin($number), 11, 0, STR_PAD_LEFT);
                }
            }
        }

        return $this->binnary;
    }

    /**
     * @inhiredoc
     */
    public function getLength(): int
    {
        return strlen($this->text);
    }
}
