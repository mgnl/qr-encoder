<?php

namespace Mgnl\QrEncoder\InputType;

class Digit extends InputTypeAbstract
{

    /**
     * @inhiredoc
     */
    public static function isValid(string &$data): bool
    {
        return is_numeric($data);
    }

    /**
     * @inhiredoc
     */
    public function getBinnary(): string
    {
        if (null === $this->binnary) {

            $this->binnary = '';

            $splited = str_split($this->text, 3);

            for ($i = 0; $i < count($splited) - 1; $i++) {
                $this->binnary .= str_pad(decbin($splited[$i]), 10, 0, STR_PAD_LEFT);
            }

            if (strlen(end($splited)) == 3) {
                $this->binnary .= str_pad(decbin($splited[$i]), 10, 0, STR_PAD_LEFT);
            } elseif (strlen(end($splited)) == 2) {
                $this->binnary .= str_pad(decbin($splited[$i]), 7, 0, STR_PAD_LEFT);
            } elseif (strlen(end($splited)) == 1) {
                $this->binnary .= str_pad(decbin($splited[$i]), 4, 0, STR_PAD_LEFT);
            }
        }

        return $this->binnary;
    }

    /**
     * @inhiredoc
     */
    public function getLength(): int
    {
        return strlen($this->text);
    }
}
