<?php

namespace Mgnl\QrEncoder;

use LogicException;

class QrLogger
{

    private $clocks = [];
    private $filePath;

    public function setFilePath(string $filePath)
    {
        $this->filePath = $filePath;
    }

    public function getFilePath(): string
    {
        return $this->filePath;
    }

    public function clock(string $clock, $retrieve = false)
    {
        $execution = $this->clocks[$clock] ?? null;

        if (true === $retrieve && null !== $execution) {

            unset($this->clocks[$clock]);

            return (microtime(true) - $execution);
        }

        if ($execution) {
            throw LogicException("This clock is already ticking, use different clock name");
        }

        $this->clocks[$clock] = microtime(true);
    }

    public function emergency($message, array $context = [])
    {
        $this->log('EMERGENCY', $message, $context);
    }

    public function alert($message, array $context = [])
    {
        $this->log('ALERT', $message, $context);
    }

    public function critical($message, array $context = [])
    {
        $this->log('CRITICAL', $message, $context);
    }

    public function error($message, array $context = [])
    {
        $this->log('ERROR', $message, $context);
    }

    public function warning($message, array $context = [])
    {
        $this->log('WARNING', $message, $context);
    }

    public function notice($message, array $context = [])
    {
        $this->log('NOTICE', $message, $context);
    }

    public function info($message, array $context = [])
    {
        $this->log('INFO', $message, $context);
    }

    public function debug($message, array $context = [])
    {
        $this->log('DEBUG', $message, $context);
    }

    public function log($level, $message, array $context = [])
    {
        $time = date('Y-m-d H:i:s');

        if (($clock = $context['clock'] ?? null)) {

            $context[$clock] = $this->clock($clock, true);

            unset($context['clock']);
        }

        $log = $this->interpolate("[$time] $level $message", $context);

        if (!empty($context)) {
            $log .= ' '.json_encode($context);
        }

        if ($this->filePath) {
            file_put_contents($this->filePath, $log.PHP_EOL, FILE_APPEND);
        }
    }

    private function interpolate($message, array &$context = [])
    {
        $replace = [];

        foreach ($context as $key => $val) {

            if (!is_array($val) && (!is_object($val) || method_exists($val, '__toString'))) {
                $replace['{'.$key.'}'] = $val;

                unset($context[$key]);
            }
        }

        return strtr($message, $replace);
    }
}
