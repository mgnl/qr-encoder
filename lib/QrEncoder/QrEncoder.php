<?php

namespace Mgnl\QrEncoder;

/**
 * @see https://stackoverflow.com/questions/5446421
 * @see http://vecg.cs.ucl.ac.uk/Projects/SmartGeometry/halftone_QR/halftoneQR_sigga13.html
 *
 * Generators:
 * @see https://www.qrstuff.com/
 *
 * @todo add interface Psr\Log\LoggerAwareInterface
 */
class QrEncoder
{

    /**
     * @var QrOptions
     */
    private $options;

    /**
     * @var QrLogger
     */
    private $logger;

    /**
     * @var QrInput
     */
    private $input;

    /**
     * @return Psr\Log\LoggerInterface
     *
     * @todo to implement PSR-3 interface
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     *
     * @param Psr\Log\LoggerInterface $logger
     *
     * @return void
     *
     * @todo to implement PSR-3 interface
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param QrOptions|null $options
     */
    public function __construct(QrOptions $options = null)
    {
        if (null === $options) {

            /** With default settings */
            $options = new QrOptions();
        }

        $this->options = $options;
    }

    /**
     * @param string $text
     */
    public function encode(string $text)
    {
        $this->input      = new QrInput($text, $this->options->getInputType());
        $inputType        = $this->input->getType();
        $textLength       = $this->input->getTextLength();
        $version          = new QrVersion($this->input, $this->options);
        $lengthIndicator  = new QrCountIndicator($version, $inputType);
        $binnaryType      = $this->input->getBinnaryType();
        $binnaryLenghtInd = $lengthIndicator->getBinnary($textLength);
        $binnaryData      = $this->input->getBinnary();

        $binnary = $this->addTerminatorAndPad(
            $binnaryType.$binnaryLenghtInd.$binnaryData,
            $version->getRequiredBites()
        );


echo $binnary;
//        $this->buildFinalStructure();
//
//        $this->placeModuleIntoMatrix();
//
//        $this->dataMasking();
//
//        $this->placeFormatAndVersion();
//
//        $this->renderImage();
    }

    private function addTerminatorAndPad(string $binnary, int $bites)
    {
        $length = strlen($binnary);
        $diff   = $bites - $length;

        if ($diff <= 4) {
            $binnary .= str_repeat('0', $diff);
        } else {
            $binnary .= '0000';
        }

        $length = strlen($binnary);

        $missingBlocks = ((int) ($length / 8) + 1) * 8 - $length;

        if ($missingBlocks) {
            $binnary .= str_repeat('0', $missingBlocks);
        }

        $length = strlen($binnary);
        $diff   = $bites - $length;

        return str_pad($binnary, $bites, '1110110000010001');
    }
}
