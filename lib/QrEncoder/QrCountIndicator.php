<?php

namespace Mgnl\QrEncoder;

class QrCountIndicator
{

    const COUNT_INDICATORS = [
        // Digit AlphaNum Kanji Byte
        [10, 9, 8, 8],
        // Version 10-26, Data type Digit AlphaNum Kanji Byte
        [12, 11, 16, 10],
        // Version 27-40, Data type Digit AlphaNum Kanji Byte
        [14, 13, 16, 12],
    ];

    private $indicator;
    private $version;
    private $dataType;

    public function __construct(QrVersion &$version, int $dataType)
    {
        $this->version = $version->getVersion();

        $this->dataType = $dataType - 1;
    }

    public function getIndicator(): int
    {
        if (null === $this->indicator) {

            if ($this->version < 10) {
                $this->indicator = self::COUNT_INDICATORS[0][$this->dataType];
            } else if ($this->version < 27) {
                $this->indicator = self::COUNT_INDICATORS[1][$this->dataType];
            } else {
                $this->indicator = self::COUNT_INDICATORS[2][$this->dataType];
            }
        }

        return $this->indicator;
    }

    public function getBinnary(int $textLength): string
    {
        return str_pad(decbin($textLength), $this->getIndicator(), 0, STR_PAD_LEFT);
    }
}
