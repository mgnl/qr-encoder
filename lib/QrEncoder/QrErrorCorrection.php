<?php
namespace Mgnl\QrEncoder;

class QrErrorCorrection
{

    const L = 1; // L - Low     ~7
    const M = 2; // M - Medium  ~15  Default
    const Q = 3; // Q - Quality ~25
    const H = 4; // H - High    ~30

}
