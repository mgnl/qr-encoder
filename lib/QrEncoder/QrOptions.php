<?php

namespace Mgnl\QrEncoder;

final class QrOptions
{

    private $inputType       = QrInput::AUTO;
    private $errorCorrection = QrErrorCorrection::Q;
    private $version         = QrVersion::AUTO;

    public function getInputType(): int
    {
        return $this->inputType;
    }

    public function setInputType(int $type): void
    {
        $this->inputType = $type;
    }

    public function getErrorCorrection(): int
    {
        return $this->errorCorrection;
    }

    public function setErrorCorrection(int $errorCorrection): void
    {
        $this->errorCorrection = $errorCorrection;
    }

    public function getVersion(): int
    {
        return $this->version;
    }

    public function setVersion(int $version): void
    {
        $this->version = $version;
    }
}
